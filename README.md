Aplikasi Microservices Sederhana Perpustakaan
-> Aplikasi pinjam buku sederhana dengan mencatat secara personal buku yang dipinjam dan dikembalikan.

Pada services ini akan mengerjakan interaksi antara orang yang meminjam buku dan status peminjaman buku,
sedangkan untuk daftar buku terdapat pada services lainnya.

Fungsi tiap tabel
- customers: melihat user, melihat user berdasarkan id, memasukkan data user, mengubah data dan menghapus data.
- borrows: melihat daftar peminjaman berdasarkan userid, menambahkan daftar pinjaman, mengubah status pinjaman

Persiapan
- Untuk mysqlapp dengan menggunakan sedikit perbedaan direktori, dimana pada folder utama terdapat main.py, config.py, dan .flaskenv
  Selanjutnya terdapat folder app, pada folder tersebut terdapat 3 subfolder yaitu controllers, models, dan routers. Pada tiap folder
  ini terdapat file .py sesuai tabel yang akan digunakan pada services ini.

Cara Penggunaan
Untuk bisa mengakses suatu user, disini membutuhkan token yang didapatkan dari sebuah id.
Penggunaan RestAPI terdapat pada controller borrows yang mengambil data dari fungsi shows() dalam bentuk json.
Ketika kita sudah mempunyai token baru bisa mengakses fitur melihat daftar peminjaman dari user yang mempunyai token ini.